# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from bs4 import BeautifulSoup
import csv
import urllib
import pandas as pd

# query the website and return the html to the variable 'page'
page = urllib.request.urlopen("https://www.careerlink.vn/tim-viec-lam-tai/da-nang/DN/c/cntt-phan-mem/19")
# parse the html using beautiful soup and store in variable 'soup'
soup = BeautifulSoup(page, 'html.parser')

# find results within table
table = soup.find('div', attrs={'class': 'list-group list-search-result-group tlp headline'})
results = table.find_all('div', attrs = {'class': 'list-group-item'})
print('Number of results', len(results))
print(results)

rows = []
output_csv = pd.DataFrame(columns = ['Title', 'Company', 'Place', 'Salary', 'Position'])

# loop over results
for result in results:
    # find all columns per result
    Title = result.find('h2', attrs={'class': 'list-group-item-heading'}).getText()
    print(Title)
    
    CompanyAndPlace = result.find('p', attrs={'class': 'priority-data'}).getText()
    CompanyAndPlace = ' '.join(CompanyAndPlace.split())
    Company = CompanyAndPlace.split('-')[0]
    Place = CompanyAndPlace.split('-')[1]
    print(Company)
    print(Place)
    
    SalaryAndPosition = result.find('div', attrs={'class': 'pull-left'}).getText()
    SalaryAndPosition = ' '.join(SalaryAndPosition.split())
    Salary = SalaryAndPosition.split('|')[0]
    Position = SalaryAndPosition.split('|')[1]
    print(Salary)
    print(Position)
    
    rows.append({'Title': Title, 'Company': Company, 'Place': Place, 'Salary': Salary, 'Position': Position})
    
output_csv = output_csv.append(rows)
output_csv.to_dense().to_csv('careerlinkJobs.csv', index = False, sep=',', encoding= 'utf-8')
